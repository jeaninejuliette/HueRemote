from phue import Bridge
import RGB_to_XY    # import the function created to convert RGB to XY
import lirc

# identify the bridge
b = Bridge('ip-of-the-bridge')

# connect to the bridge (first press button on bridge)
# this only has to be done the first time you setup the connection
#b.connect()

# get the names of all the lights
#print b.get_light_objects('name')
# Zithoek, Zithoek bloom
# Raamkant, Midden, Keukenkant
# Slaapkamer

# get the names of the light groups
#print b.get_group()
# Zithoek, Eetkamer, Slaapkamer

# create a connection to LIRC
sockid = lirc.init("Hue", blocking = True)

# create the main loop
while True:
    # get the button that is pressed on the remote
    scene = lirc.nextcode()
    #scene = ['volumedown']
    if scene:
        # check which button is pressed, i.e. which is the desired scene
        if scene[0] == 'volumedown':
            print 'make all lights a little bit less bright'
            for light in b.lights:
                #print light.brightness
                light.brightness = light.brightness - 25

        elif scene[0] == 'volumeup':
            print 'make all lights a little bit brighter'
            for light in b.lights:
                #print light.brightness
                light.brightness = light.brightness + 25

        elif scene[0] == '1':
            print 'time for dinner'
            b.set_group('Eetkamer', {'on':True, 'bri' :254, 'xy':RGB_to_XY.main(226, 198, 13)})
            b.set_group('Zithoek', {'on':True, 'bri':150, 'xy':RGB_to_XY.main(226, 198, 13)})
            b.set_group(' Slaapkamer' , {'on':False})   
                        
        elif scene[0] == '2':
            print 'get to work'
            b.set_group('Eetkamer', {'on':True, 'bri' :254, 'xy':RGB_to_XY.main(252, 250, 237)})
            b.set_group('Zithoek', {'on':False})
            b.set_group(' Slaapkamer' , {'on':False})
        
        elif scene[0] == '3':
            print 'colors!'
            b.set_light('Raamkant', {'on':True, 'bri':200, 'xy':RGB_to_XY.main(206, 37, 121)})
            b.set_light('Midden', {'on':True, 'bri':200, 'xy':RGB_to_XY.main(38, 70, 201)})
            b.set_light('Keukenkant', {'on':True, 'bri':200, 'xy':RGB_to_XY.main(206, 37, 121)})
            b.set_light('Zithoek', {'on':True, 'bri':150, 'xy':RGB_to_XY.main(38, 70, 201)})
            b.set_light('Zithoek bloom', {'on':True, 'bri':300, 'xy':RGB_to_XY.main(206, 37, 121)})
            b.set_group(' Slaapkamer' , {'on':False})
        
        elif scene[0] == '4':
            print 'go green'
            XY = RGB_to_XY.main(2, 124, 29)
            b.set_light('Raamkant', {'on':True, 'bri':200, 'xy':RGB_to_XY.main(2, 124, 29)})
            b.set_light('Midden', {'on':True, 'bri':200, 'xy':RGB_to_XY.main(16, 102, 35)})
            b.set_light('Keukenkant', {'on':True, 'bri':200, 'xy':RGB_to_XY.main(2, 104, 24)})
            b.set_light('Zithoek', {'on':True, 'bri':150, 'xy':RGB_to_XY.main(16, 86, 31)})
            b.set_light('Zithoek bloom', {'on':True, 'bri':300, 'xy':RGB_to_XY.main(1, 35, 8)})
            b.set_group(' Slaapkamer' , {'on':False})
        
        elif scene[0] == '5':
            print 'hot pink'
            XY = 
            b.set_group('Eetkamer', {'on':True, 'bri' :400, 'xy':RGB_to_XY.main(193, 17, 123)})
            b.set_group('Zithoek', {'on':True, 'bri' :400, 'xy':RGB_to_XY.main(193, 17, 123)})
            b.set_group(' Slaapkamer' , {'on':False})
        
        elif scene[0] == '6':
            print 'feeling blue'
            b.set_group('Eetkamer', {'on':True, 'bri' :100, 'xy':RGB_to_XY.main(7, 104, 239)})
            b.set_group('Zithoek', {'on':True, 'bri' :100, 'xy':RGB_to_XY.main(7, 104, 239)})
            b.set_group(' Slaapkamer' , {'on':False})
        
        elif scene[0] == '7':
            print 'more colors'
            b.set_light('Raamkant', {'on':True, 'bri':200, 'xy':RGB_to_XY.main(219, 6, 20)})
            b.set_light('Midden', {'on':True, 'bri':200, 'xy':RGB_to_XY.main(231, 239, 4)})
            b.set_light('Keukenkant', {'on':True, 'bri':200, 'xy':RGB_to_XY.main(237, 147, 2)})
            b.set_light('Zithoek', {'on':True, 'bri':150, 'xy':RGB_to_XY.main(237, 193, 1)})
            b.set_light('Zithoek bloom', {'on':True, 'bri':300, 'xy':RGB_to_XY.main(219, 6, 20)})
            b.set_group(' Slaapkamer' , {'on':False})
        
        elif scene[0] == '8':
            print 'we love Holland'
            b.set_group('Eetkamer', {'on':True, 'bri' :254, 'xy':RGB_to_XY.main(239, 119, 7)})
            b.set_group('Zithoek', {'on':True, 'bri' :254, 'xy':RGB_to_XY.main(239, 119, 7)})
            b.set_group(' Slaapkamer' , {'on':False})
        
        elif scene[0] == '9':
            print 'time for bed'
            b.set_group('Eetkamer', {'on':False})
            b.set_group('Zithoek', {'on':False})
            b.set_group(' Slaapkamer' , {'on':True, 'bri':150, 'xy':RGB_to_XY.main(244, 226, 66)})
        
        elif scene[0] == '0':
            print 'all lights off'
            b.set_group('Eetkamer', {'on':False})
            b.set_group('Zithoek', {'on':False})
            b.set_group(' Slaapkamer' , {'on':False})
