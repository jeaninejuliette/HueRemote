# Date      :   21 Feb 2016
# Author    :   Jeanine Schoonemann
# Title     :   RGB to XY converter
# Purpose   :   get the XY color from the RGB code so you
#               can pass it on to the Philips Hue lights

import sys

def main(R, G, B):
    # the RGB code
    #R = 255
    #G = 0
    #B = 100
    
    # convert the RGB code to a number between 0 and 1
    # dividing by 255.0 makes sure the outcome is a float
    red = R/255.0
    green = G/255.0
    blue = B/255.0

    # convert the RGB between 0 and 1 to XYZ
    X = red * 0.664511 + green * 0.154324 + blue * 0.162028
    Y = red * 0.283881 + green * 0.668433 + blue * 0.047685
    Z = red * 0.000088 + green * 0.072310 + blue * 0.986039

    # convert XYZ to XY
    X = X / (X + Y + Z)
    Y = Y / (X + Y + Z)

    return [X, Y]
